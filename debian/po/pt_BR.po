# Debconf translations for w3c-linkchecker.
# Copyright (C) 2011 Nicholas Bamber <nicholas@periapt.co.uk>
# Copyright (C) 2011 Justin B Rye <jbr@edlug.org.uk>
# Adriano Rafael Gomes <adrianorg@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: w3c-linkchecker 4.8-1\n"
"Report-Msgid-Bugs-To: w3c-linkchecker@packages.debian.org\n"
"POT-Creation-Date: 2011-01-29 03:15+0000\n"
"PO-Revision-Date: 2011-05-27 11:18-0300\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@gmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Host name for W3C LinkChecker service:"
msgstr "Nome da máquina para o serviço W3C LinkChecker:"

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"Please specify the fully qualified domain name that the w3c-linkchecker "
"service should be remotely accessible on, if any. By default it will only be "
"available on localhost."
msgstr ""
"Por favor, especifique o nome totalmente qualificado no qual o serviço w3c-"
"linkchecker deverá ser acessível remotamente, se for o caso. Por padrão, ele "
"estará disponível apenas na máquina local."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Allow private IP addresses?"
msgstr "Permitir endereços IP privados?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"Please specify whether w3c-linkchecker should permit validation of websites "
"on private networks. By default it will only permit public IP addresses."
msgstr ""
"Por favor, especifique se o w3c-linkchecker deverá permitir a validação de "
"websites em redes privadas. Por padrão, ele apenas permitirá endereços IP "
"públicos."
