# Translation of the w3c-linkchecker package to French
# Copyright (C) 2011 Julien Patriarca <patriarcaj@gmail.com>
# This file is distributed under the same license as the w3c-linkchecker package
msgid ""
msgstr ""
"Project-Id-Version: w3c-linkchecker\n"
"Report-Msgid-Bugs-To: w3c-linkchecker@packages.debian.org\n"
"POT-Creation-Date: 2011-01-29 03:15+0000\n"
"PO-Revision-Date: 2011-02-13 20:17+0100\n"
"Last-Translator: Julien Patriarca <patriarcaj@gmail.com>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org> \n"
"Language: fr \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Host name for W3C LinkChecker service:"
msgstr "Nom d'hôte pour le service W3C LinkChecker :"

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"Please specify the fully qualified domain name that the w3c-linkchecker "
"service should be remotely accessible on, if any. By default it will only be "
"available on localhost."
msgstr ""
"Veuillez indiquer le nom de domaine pleinement qualifié où le service w3c-"
"linkchecker sera accessible à distance si actif. Par défaut il sera "
"seulement accessible en local."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Allow private IP addresses?"
msgstr "Autoriser les adresses IP privées ?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"Please specify whether w3c-linkchecker should permit validation of websites "
"on private networks. By default it will only permit public IP addresses."
msgstr ""
"Veuillez indiquer si w3c-linkchecker devra autoriser la validation de sites "
"internet sur les réseaux privés. Par défaut il n'autorisera uniquement que "
"les adresses IPs publiques."
